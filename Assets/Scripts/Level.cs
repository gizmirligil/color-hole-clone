﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Level", menuName = "ScriptableObjects/Level", order = 1)]
public class Level : ScriptableObject
{
        public int levelid1;
        public int levelid2;
        public GameObject stage1;
        public GameObject stage2;
        public Color tableColor;
        public Color borderColor;
        public Color loseColor;
    
}
