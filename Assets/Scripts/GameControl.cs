﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Debug = UnityEngine.Debug;
using Random = System.Random;

public class GameControl : MonoBehaviour
{
    private enum State
    {
        NEXT,
        START,
        LOSE,
        CONTINUE,
        READY,
        DEFAULT
    }
    

    [SerializeField] private GameObject _gate;
    [SerializeField] private Material borderMat;
    [SerializeField] private Material tableMat;
    [SerializeField] private Material loseMat;
    [SerializeField] private ParticleSystem confetti;
    [SerializeField] private GameObject stage1;
    [SerializeField] private GameObject stage2;
    [SerializeField] private GameObject _bridge;
    [SerializeField] private Camera mainCam;
    [SerializeField] private GameObject startCamPos;
    [SerializeField] private GameObject hole;
    [SerializeField] private GameObject startPos;
    [SerializeField] private TouchDrag _touchDrag;
    [SerializeField] private Score _score;
    [SerializeField] private ForcedMovement _forced;
    [SerializeField] private GameObject bridgeSpheres;
    [SerializeField] private TextMeshProUGUI leftLevelID;
    [SerializeField] private TextMeshProUGUI rightLevelID;
    [SerializeField] private GameObject swipe;
    [SerializeField] private GameObject loseUI;
    [SerializeField] private Trigger _trigger;
    [SerializeField] private Level[] levels;
    private State state;
    public int _currentLevelNo = 0;
    private Level currentLevel;
    
    private void Awake()
    {
        NewLevel();
        state = State.START;

    }
    public void NewLevel()
    {
        state = State.START;
        swipe.SetActive(true);
        if (_currentLevelNo <= 2)
        {
            currentLevel = levels[_currentLevelNo];
        }
        else
        {
            //return to level1
            _currentLevelNo = 0;
            currentLevel = levels[_currentLevelNo];
        }
        

        StartCoroutine(DestroyAll());
        StartCoroutine(StartLevel());
        _touchDrag._topBorder = 26.5f;
        _touchDrag._bottomBorder = -25.5f;
        hole.transform.position = startPos.transform.position;
        mainCam.transform.position = startCamPos.transform.position;
        _gate.transform.position = new Vector3(0,3,29.5f);



        var bridgeblocks =
        Instantiate(bridgeSpheres,new Vector3(0, 0, 49), Quaternion.identity);
        bridgeblocks.transform.parent = _bridge.transform;
        
        var stage1blocks = Instantiate(currentLevel.stage1, stage1.transform);
        stage1blocks.transform.parent = stage1.transform;
        var stage2blocks = Instantiate(currentLevel.stage2, stage2.transform);
        stage2blocks.transform.parent = stage2.transform;
        stage2.SetActive(false);
        
        
        borderMat.color = currentLevel.borderColor;
        tableMat.color = currentLevel.tableColor;
        loseMat.color = currentLevel.loseColor;
        
        
        leftLevelID.text = currentLevel.levelid1.ToString();
        rightLevelID.text = currentLevel.levelid2.ToString();
        
        
        

        


    }

    public IEnumerator NextStage()
    {
        if (state == State.LOSE) ;

        else
        {

            state = State.NEXT;
            _trigger.transition = true;
            _forced.StartCoroutine("NextStage");
            while (_gate.transform.position.y >= -3)
            {
                _gate.transform.Translate(0, -3 * Time.deltaTime, 0);
                yield return null;
            }

            stage2.SetActive(true);
            _score._stage = 1;
            _score.CountStage(1);
            state = State.DEFAULT;
            yield break;
        }
    }

    public IEnumerator Lose()
    {
        if (state == State.NEXT) ;
        else
        {
            state = State.LOSE;
            loseUI.SetActive(true);
            _touchDrag._lockControl = true;
            yield break;
        }

    }

    public void Restart()
    {
        state = State.CONTINUE;
        loseUI.SetActive(false);
        _touchDrag._lockControl = false;
        NewLevel();
    }

    private IEnumerator DestroyAll()
    {
        foreach (Transform child in stage1.transform) {
            Destroy(child.gameObject);
        }
        foreach (Transform child in stage2.transform) {
            Destroy(child.gameObject);
        }
        foreach (Transform child in _bridge.transform) {
            Destroy(child.gameObject);
        }

        state = State.READY;
        yield break;
    }

    private IEnumerator StartLevel()
    {
        yield return new WaitUntil(()=>state == State.READY);
        _score.Init();
        _score.CountStage(0);
        state = State.DEFAULT;
        yield break;
    }

    public IEnumerator Win()
    {
        state = State.NEXT;
        confetti.Play();
        yield return new WaitForSeconds(2);
        NewLevel();
        yield break;
    }
}
