﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchDrag : MonoBehaviour
{
    public float _leftBorder = -10.5f;
    public float _rightBorder = 10.5f;
    public float _topBorder = 26.5f;
    public float _bottomBorder = -25.5f;
    public float speed = 5f;
    public bool _lockControl=false;

    [SerializeField] private GameObject _swipe;
    
    // Start is called before the first frame update
    void Start()
    {
     _leftBorder = -10.5f;
     _rightBorder = 10.5f;
     _topBorder = 26.5f;
    _bottomBorder = -25.5f;
    }

    // Update is called once per frame
    
    void FixedUpdate()
    {
        
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved && !_lockControl)
        {
            _swipe.SetActive(false);
            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
            transform.position = new Vector3(Mathf.Clamp(transform.position.x+(touchDeltaPosition.x* speed * Time.deltaTime/Screen.dpi),_leftBorder ,_rightBorder ),
                0, Mathf.Clamp(transform.position.z +(touchDeltaPosition.y * speed * Time.deltaTime/Screen.dpi),_bottomBorder ,_topBorder ));
            
        }
    }
}
