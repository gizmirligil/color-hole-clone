﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
   public bool transition = false;
   private void OnTriggerEnter(Collider other)
   {
      if (transition && other.CompareTag("LoseCube"))
      {
      }
      else if (other.gameObject.layer==9)
      {
         other.gameObject.GetComponent<Rigidbody>().isKinematic = false;
         other.gameObject.layer = 8;
      }
   }

   private void OnTriggerExit(Collider other)
   {
      if (other.gameObject.layer==8)
      {
         
         other.gameObject.layer = 9;
      }
   }
}
