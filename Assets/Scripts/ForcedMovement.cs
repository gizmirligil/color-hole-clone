﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ForcedMovement : MonoBehaviour
{
    [SerializeField] private GameObject _finish;
    [SerializeField] private GameObject _nextStart;
    [SerializeField] private Camera _mainCamera;
    [SerializeField] private GameObject _nextCamPos;
    [SerializeField] private TouchDrag _touchDrag;
    [SerializeField] private Trigger _trigger;
    
    
    private void Update()
    {
        
    }

    IEnumerator NextStage()
    {
        _touchDrag._lockControl = true;
        while (Mathf.Abs(transform.position.x - _finish.transform.position.x)>=0.5)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(_finish.transform.position.x, transform.position.y, transform.position.z), Time.deltaTime);
            yield return null;
        }
        
        
        while (Mathf.Abs(transform.position.z - _nextStart.transform.position.z) >= 0.5)
        {
            
            transform.position = Vector3.Lerp(transform.position, new Vector3(_nextStart.transform.position.x, transform.position.y, _nextStart.transform.position.z),
                Time.deltaTime);
            _mainCamera.transform.position = Vector3.Lerp(_mainCamera.transform.position, _nextCamPos.transform.position, Time.deltaTime);
            yield return null;
        }
        
        
        _touchDrag._topBorder = 126.5f;
        _touchDrag._bottomBorder = 74.5f;
        _touchDrag._lockControl = false;
        _trigger.transition = false;
        
        yield break;
    }
}
