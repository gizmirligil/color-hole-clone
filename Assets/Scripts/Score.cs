﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    
    public int _score1 = 0;
    public int _maxScore1 = 0;
    public int _score2 = 0;
    public int _maxScore2 = 0;
    [SerializeField] private Slider _scoreSlider1;
    [SerializeField] private Slider _scoreSlider2;
    [SerializeField] private GameControl gameControl;
    [SerializeField] private GameObject stage1;
    [SerializeField] private GameObject stage2;
    public int _stage = 0;
    private void Start()
    {
        
        
    }

    public void Init()
    {
        _stage = 0;
        _score1 = 0;
        _maxScore1 = 0;
        _score2 = 0;
        _maxScore2 = 0;
        _scoreSlider1.value = 0;
        _scoreSlider2.value = 0;
    }
    public void CountStage(int stage)
    {
        
        foreach (GameObject scoreCube in GameObject.FindGameObjectsWithTag("ScoreCube"))
        {
            if (stage==0)
            {
                _maxScore1++;
                
                
            }
            if (stage==1)
            {
                _maxScore2++;
                
                
            }
            
        }
        
        _scoreSlider1.maxValue = _maxScore1;
        
        _scoreSlider2.maxValue = _maxScore2;
    }

 
    private void Update()
    {
        _scoreSlider1.value = _score1;
        _scoreSlider2.value = _score2;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("ScoreCube"))
        {
            ScoreUp();
            
            StartCoroutine(Deactivate(other.gameObject));
        }

        if (other.gameObject.CompareTag("LoseCube"))
        {
            Lose();
            StartCoroutine(Deactivate(other.gameObject));
        }
        
        if (other.gameObject.CompareTag("BridgeSphere"))
        {
            Destroy(other.gameObject);
        }
        
    }

    IEnumerator Deactivate(GameObject _gameObject)
    {
        yield return new WaitForSeconds(1);
        if(_gameObject!=null)_gameObject.SetActive(false);
        

    }

    private void ScoreUp()
    {
        if (_stage == 0)
        {
            _score1++;
            if (_score1 == _maxScore1)
            {
                gameControl.StartCoroutine("NextStage");
            }
        }

        if (_stage == 1)
        {
            _score2++;
            if (_score2 == _maxScore2)
            {
                gameControl._currentLevelNo++;
                gameControl.StartCoroutine("Win");
            }
        }

    }

    private void Lose()
    {
        gameControl.StartCoroutine("Lose");
    }
}
